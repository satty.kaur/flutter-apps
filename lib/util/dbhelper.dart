import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_apps/model/todo.dart';


//This class is used to make CRUD operations to the database. Because we only want to make instantiate this class once
// We need to make it a singleton. A singleton ensures that there is only one instance of the class.

class DbHelper {

  //SINGLETON - the DbHelper ensure that there is only once instance of the database.
  static final DbHelper _dbHelper = DbHelper._internal();

  String tblTodo = "todo";
  String colId = "id";
  String colTitle = "title";
  String colDescription = "description";
  String colPriority = "priority";
  String colDate = "date";

  //NAMED CONSTRUCTOR
  DbHelper._internal();

  //UNAMED CONSTRUCTOR
  factory DbHelper() {
    return _dbHelper;
  }

  //DATABASE VARIABLE - private variable, _db is used to store the database
  static Database _db;

  //GETTER METHOD - get the database, if the database == null, then call the initializeDb() method to return the database
  Future<Database> get db async {
    if(_db ==null) {
      _db = await initializeDb();
    }
    return _db;
  }

  //CONNECT TO DATABASE - if the database doesnt exist then calls the _createDb() method.
  Future<Database> initializeDb() async {
    Directory dir = await getApplicationDocumentsDirectory();
    String path = dir.path + "todos.db";
    var dbTodos = await openDatabase(path, version: 1, onCreate: _createDb);
    return dbTodos;
  }

  //CREATE DATABASE - create a database with the following columns
  void _createDb(Database db, int newVersion) async {
    await db.execute(
      "CREATE TABLE $tblTodo($colId INTEGER PRIMARY KEY, "
          "$colTitle TEXT, $colDescription TEXT, $colPriority INTEGER, $colDate TEXT)"
    );
  }

  //INSERT RECORD IN DATABASE - insert into todos database and call the toMap method to map values to the column keys
  Future<int> insertTodo(Todo todo) async {
    Database db = await this.db;
    var result = await db.insert(tblTodo, todo.toMap());
    return result;
  }

  //SELECT ALL RECORDS IN DATABASE - return them as a list in order of priority
  Future<List> getTodos() async {
    Database db = await this.db;
    var result = await db.rawQuery("SELECT * FROM $tblTodo order by $colPriority ASC");
    return result;
  }

  //RETURN COUNT - returns and int for the number of items in the database.
  Future<int> getCount() async {
    Database db = await this.db;
    var result = Sqflite.firstIntValue(
      await db.rawQuery("select count (*) from $tblTodo")
    );
    return result;
  }

  //UPDATE RECORD - pass in a TODOS object, call toMap to map to key columns, update record where id matches the TODOS object being inserted
  Future<int> updateTodo(Todo todo) async {
    var db = await this.db;
    var result = await db.update(tblTodo, todo.toMap(),
    where: "$colId = ?", whereArgs: [todo.id]);
    return result;
  }

  //DELETE RECORD - pass in a id number and perform SQL query to delete record with match id.
  Future<int> deleteTodo(int id) async {
    int result;
    var db = await this.db;
    result = await db.rawDelete("DELETE FROM $tblTodo WHERE $colId = $id");
    return result;
  }


}