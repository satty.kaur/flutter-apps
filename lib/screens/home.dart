import 'dart:ui';

import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
   return Material(
     color: Colors.amber,
     child: Center(
       child: Text(
         sayHello(),
         textDirection: TextDirection.ltr,
         style: TextStyle(color: Colors.white, fontSize: 26.0),
       ),
     ),
   );
  }

  String sayHello() {
    String hello;
    DateTime now = new DateTime.now();
    int hour = now.hour;
    int min = now.minute;
    if (hour < 12) {
      hello = "Good morning";
    } else if (hour < 18) {
      hello = "Good afternoon";
    } else {
      hello = "Good evening";
    }
    String minutes = (min < 10) ? "0" + min.toString() : min.toString();

    return hello + "!\n" + "The time is " + hour.toString() + ":" + minutes;
  }

}

