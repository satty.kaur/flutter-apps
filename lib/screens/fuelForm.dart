import 'package:flutter/material.dart';


class FuelForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _FuelFormState();
}

class _FuelFormState extends State<FuelForm> {
  String result = '';
  final _currencies = ['Dollars', 'Euro', 'Pounds'];
  final double form_distance = 5;
  String _currency = 'Dollars';
  TextEditingController distanceController = TextEditingController();
  TextEditingController avgController = TextEditingController();
  TextEditingController priceController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.subtitle1;
    return Scaffold(
      appBar: AppBar(
        title: Text("Trip Calculator"),
      ),
      body: Container(
        padding: EdgeInsets.all(15),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Text("Road Trip Calculator",
                style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold
                ),),
              CarImage(),
              Padding(
                  padding: EdgeInsets.all(10),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                          child: TextField(
                            controller: distanceController,
                            decoration: InputDecoration(
                                labelText: 'Total Distance',
                                hintText: 'e.g. 124 miles',
                                labelStyle: textStyle,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0))),
                            keyboardType: TextInputType.number,
                          )),
                      Container(width: form_distance * 5),
                      Expanded(
                          child: TextField(
                            controller: avgController,
                            decoration: InputDecoration(
                                labelText: 'Distance per gallon',
                                hintText: 'e.g. 45 miles',
                                labelStyle: textStyle,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0))),
                            keyboardType: TextInputType.number,
                          ))
                    ],
                  )),
              Padding(
                  padding: EdgeInsets.all(10),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: TextField(
                          controller: priceController,
                          decoration: InputDecoration(
                              labelText: 'Price per gallon',
                              hintText: 'e.g. 1.65',
                              labelStyle: textStyle,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5.0))),
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      Container(width: form_distance * 5),
                      Expanded(
                        child: DropdownButton<String>(
                          items: _currencies.map((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          value: _currency,
                          onChanged: (String value) {
                            _onDropDownChanged(value);
                          },
                        ),
                      ),
                    ],
                  )),
              Row(
                children: <Widget>[
                  Expanded(
                    child: RaisedButton(
                      color: Theme.of(context).accentColor,
                      textColor: Colors.white,
                      onPressed: () {
                        setState(() {
                          result = _calculate();
                        });
                      },
                      child: Text(
                        "Submit",
                        textScaleFactor: 1.5,
                      ),
                    ),
                  ),
                  Container(width: form_distance * 5),
                  Expanded(
                    child: RaisedButton(
                      color: Theme.of(context).accentColor,
                      textColor: Colors.white,
                      onPressed: () {
                        setState(() {
                          _reset();
                        });
                      },
                      child: Text(
                        "Reset",
                        textScaleFactor: 1.5,
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: Text(
                    result,
                    style: TextStyle(fontSize: 20),
                  ))
            ],
          ),
        ),
      ),
    );
  }


  _onDropDownChanged(String val) {
    setState(() {
      this._currency = val;
    });
  }

  void _reset() {
    distanceController.text = "";
    priceController.text = "";
    avgController.text = "";
    setState(() {
      result = '';
    });
  }

  String _calculate() {

    double _distance = double.parse(distanceController.text);
    double _consumption = double.parse(avgController.text);
    double _fuelCost = double.parse(priceController.text);
    double _totalCost = _distance / _consumption * _fuelCost;
    String result = "";

    if (_currency == 'Dollars') {
      result = 'Total Cost: \$' + _totalCost.toStringAsFixed(2);
    } else if (_currency == 'Euro') {
      result = 'Total Cost: €' + _totalCost.toStringAsFixed(2);
    } else {
      result = 'Total Cost: £' + _totalCost.toStringAsFixed(2);
    }

    return result;
  }
}

class CarImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AssetImage carAsset = AssetImage('images/roadTripCalculator.jpg');
    Image image = Image(image: carAsset);
    return Container(
        child: image,
        margin: EdgeInsets.only(top: 20, bottom: 30)
    );
  }
}
