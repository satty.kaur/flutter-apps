import 'package:flutter/material.dart';
import 'package:flutter_apps/model/todo.dart';
import 'package:flutter_apps/util/dbhelper.dart';
import 'package:intl/intl.dart';

DbHelper helper = DbHelper();

final List<String> choices = const <String> [
  'Save Todo & Back',
  'Delete Todo',
  'Back to List'
];
const mnuSave = 'Save Todo & Back';
const mnuDelete = 'Delete Todo';
const mnuBack = 'Back to List';


class TodoDetail extends StatefulWidget {
  final Todo todo;
  TodoDetail(this.todo);

  @override
  State<StatefulWidget> createState() => TodoDetailState(todo);

}

class TodoDetailState extends State {
  Todo todo;
  TodoDetailState(this.todo);
  final _priorities = ["High", "Medium", "Low"];
  String _priority = "Low";
  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    titleController.text = todo.title;
    descriptionController.text = todo.description;
    TextStyle textStyle = Theme.of(context).textTheme.subtitle2;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(todo.title),
        actions: [
          PopupMenuButton<String>(
            onSelected: select,
            itemBuilder: (BuildContext context) {
              return choices.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          )
        ],
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 45, left: 10, right: 10),
        child: ListView(children: [
          Column(
          children: [
            TextField(
              controller: titleController,
              style: textStyle,
              onChanged: (value) => updateTitle(),
              decoration: InputDecoration(
                labelText: "Title",
                labelStyle: textStyle,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0)
                )
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20, bottom: 25),
              child: TextField(
                controller: descriptionController,
                style: textStyle,
                onChanged: (value) => updateDescription(),
                decoration: InputDecoration(
                    labelText: "Description",
                    labelStyle: textStyle,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0)
                    )
                ),
              )),
            Align(
                alignment: Alignment.topLeft,
                child: Text("Priority")),
            ListTile(title:DropdownButton<String>(
              items: _priorities.map((String value) {
                return DropdownMenuItem<String> (
                  value: value,
                  child: Text(value),
                );
              }).toList(),
              style: textStyle,
              value:retrievePriority(todo.priority),
              onChanged: (value)=>updatePriority(value),
            )),
            Padding(
              padding: EdgeInsets.only(left: 50, right: 50, bottom: 50),
              child: Row(children: [
                Expanded(
                  child: RaisedButton(
                    color: Theme.of(context).accentColor,
                    textColor: Colors.white,
                    onPressed: save,
                    child: Text("Save"),
                  ),
                ),
                Container(width: 20),
                Expanded(
                  child: RaisedButton(
                    color: Theme.of(context).accentColor,
                    textColor: Colors.white,
                    onPressed: ()  {
                      delete();
                    },
                    child: Text("Delete"),
                  ),
                )
              ],),
            )
          ],
          )
        ],)
      )
    );
  }

  void select (String val) async {

    switch(val) {
      case mnuSave:
        save();
        break;
      case mnuDelete:
        delete();
        break;
      case mnuBack:
        Navigator.pop(context, true);
        break;
    }
  }

  void delete() async {
    Navigator.pop(context, true);
    if(todo.id == null) {
      return;
    }
    int result = await helper.deleteTodo(todo.id);
    if(result != 0) {
      AlertDialog alert = AlertDialog(
        title: Text("Delete Todo"),
        content: Text("Todo has been deleted"),
      );
      showDialog(
          context: context,
          builder: (_) => alert);
    }
  }

  void save() {
    todo.date = new DateFormat.yMMMMd('en_US').format(DateTime.now());
    if(todo.id != null) {
      helper.updateTodo(todo);
    } else {
      helper.insertTodo(todo);
    }
    Navigator.pop(context, true);
  }

  String retrievePriority(int value) {
    return _priorities[value-1];
  }

  void updatePriority(String value) {
    switch (value) {
      case "High":
        todo.priority = 1;
        break;
      case "Medium":
        todo.priority = 2;
        break;
      case "Low":
        todo.priority = 3;
        break;
    }
    setState(() {
      _priority=value;
    });
  }

  void updateTitle() {
    todo.title = titleController.text;
  }

  void updateDescription() {
    todo.description = descriptionController.text;
  }

}