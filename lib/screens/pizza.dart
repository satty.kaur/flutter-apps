import 'dart:ui';
import 'package:flutter/material.dart';


//------------STATELESS WIDGET EXAMPLE------------------
class Pizza extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.only(top: 30.0, left: 20, right: 20),
            color: Colors.teal,
            child: Column(
              children: <Widget>[
                Container(
                  child: Text(
                    "Pizza",
                    style: TextStyle(
                        color: Colors.white, decoration: TextDecoration.none),
                  ),
                ),
                PizzaImage(),
                Row(
                  children: <Widget>[
                    Expanded(
                        child: Text(
                      "Margarita",
                      textDirection: TextDirection.ltr,
                      style: TextStyle(
                          fontSize: 30.0,
                          color: Colors.white,
                          decoration: TextDecoration.none,
                          fontFamily: 'Oxygen',
                          fontWeight: FontWeight.normal),
                    )),
                    Expanded(
                        child: Text(
                      "Tomato, Mozzarella, Basil",
                      textDirection: TextDirection.ltr,
                      style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.white,
                          decoration: TextDecoration.none,
                          fontFamily: 'Oxygen',
                          fontWeight: FontWeight.normal),
                    )),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                        child: Text(
                      "Marinara",
                      textDirection: TextDirection.ltr,
                      style: TextStyle(
                          fontSize: 30.0,
                          color: Colors.white,
                          decoration: TextDecoration.none,
                          fontFamily: 'Oxygen',
                          fontWeight: FontWeight.normal),
                    )),
                    Expanded(
                        child: Text(
                      "Tomato, Garlic",
                      textDirection: TextDirection.ltr,
                      style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.white,
                          decoration: TextDecoration.none,
                          fontFamily: 'Oxygen',
                          fontWeight: FontWeight.normal),
                    )),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                        child: Text(
                      "Pepperoni",
                      textDirection: TextDirection.ltr,
                      style: TextStyle(
                          fontSize: 30.0,
                          color: Colors.white,
                          decoration: TextDecoration.none,
                          fontFamily: 'Oxygen',
                          fontWeight: FontWeight.normal),
                    )),
                    Expanded(
                        child: Text(
                      "Tomato, Mozzarella, Pepperoni",
                      textDirection: TextDirection.ltr,
                      style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.white,
                          decoration: TextDecoration.none,
                          fontFamily: 'Oxygen',
                          fontWeight: FontWeight.normal),
                    )),
                  ],
                ),
                OrderButton(),
              ],
            )));
  }
}

class PizzaImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AssetImage pizzaAsset = AssetImage('images/pizza.jpeg');
    Image image = Image(image: pizzaAsset);
    return Container(
      child: image,
      margin: EdgeInsets.only(top: 30, bottom: 50),
      decoration: borderDecoration(),
    );
  }
}

class OrderButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var button = Container(
      alignment: Alignment.center,
      margin: EdgeInsets.only(top: 50.0),
      child: RaisedButton(
        child: Text("Order"),
        color: Colors.tealAccent,
        elevation: 5.0,
        onPressed: () {
          order(context);
        },
      ),
    );
    return button;
  }

  void order(BuildContext context) {
    var alert = AlertDialog(
      title: Text(
        "Order Complete",
        textAlign: TextAlign.center,
      ),
      content: Text(
        "Thank You For Your Order",
        textAlign: TextAlign.center,
      ),
    );
    showDialog(context: context, builder: (BuildContext context) => alert);
  }
}

BoxDecoration borderDecoration() {
  return BoxDecoration(border: Border.all(width: 3, color: Colors.white));
}
