import 'package:flutter/material.dart';
import 'screens/animationPage.dart';
import 'screens/home.dart';
import 'screens/pizza.dart';
import 'screens/fuelForm.dart';
import 'screens/todolist.dart';


//---------HELLO WORLD EXAMPLE-----------------
//void main() => runApp(new HelloFlutterApp());
//class HelloFlutterApp extends StatelessWidget{
//  @override
//  Widget build(BuildContext context) {
//   return MaterialApp(
//     debugShowCheckedModeBanner: false,
//     title: "Hello Flutter App",
//     home: Scaffold(appBar: AppBar(
//       backgroundColor: Color(0xffc70244),
//       title: Text("Flutter Demo"),),
//         body:Home()
//     ),
//   );
//  }
//}

//---------PIZZA PAGE EXAMPLE (STATELESS)-----------------
//void main() {
//  runApp(new MaterialApp(
//    debugShowCheckedModeBanner: false,
//    title: "Simple Layouts",
//    home: new Pizza(),
//  ));
//}


//--------TRIP COST EXAMPLE (STATEFUL)---------------------
//void main() => runApp(new MyApp());
//
//class MyApp extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return new MaterialApp(
//      title: "Road Trip Calculator",
//      theme:
//      new ThemeData(primaryColor: Colors.indigo, accentColor: Colors.amber),
//      debugShowCheckedModeBanner: false,
//      home: new FuelForm(),
//    );
//  }
//}


//--------TODOs EXAMPLE (DATABASE)---------------------
//void main() => runApp(new MyApp());
//
//class MyApp extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//
//    return new MaterialApp(
//      title: "Todo",
//      theme: new ThemeData(primaryColor: Colors.indigo, accentColor: Colors.amber),
//      home: new MyHomePage(title: "Todo App"),
//      debugShowCheckedModeBanner: false,
//    );
//  }
//
//}
//
//class MyHomePage extends StatefulWidget {
//  MyHomePage({Key key, this.title}) :super(key: key);
//  final String title;
//
//  @override
//  State<StatefulWidget> createState() => new _MyHomePageState();
//}
//
//class _MyHomePageState extends State<MyHomePage> {
//  @override
//  Widget build(BuildContext context) {
//    return new Scaffold(
//      appBar: new AppBar(
//        title: new Text(widget.title),
//      ),
//      body: TodoList(),
//    );
//  }
//
//}



//--------GESTURES & ANIMATIONS EXAMPLE---------------------
void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: "Gestures & Animations",
      theme: new ThemeData(primaryColor: Colors.teal, accentColor: Colors.tealAccent),
      home: new AnimationPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}
