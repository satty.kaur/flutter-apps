

//TODO MODEL CLASS
class Todo {

  // PROPERTIES: the underscore marks the variable as private
  int _id;
  String _title;
  String _description;
  String _date;
  int _priority;


  /*CONSTRUCTORS: optional parameters are enclosed in square brackets. The first constructor is when you dont have an id
                  the other constructor is when you do have the id. You can only have one class name constructor in a class
                  so we call the other constructor using a name to describe E.g 'Todo.withId'*/
  Todo(this._title, this._date, this._priority, [this._description]);
  Todo.withId(this._id, this._title, this._date, this._priority, [this._description]);

  //GETTERS - we can use the fat arrow syntax when we dont need to manipulate the property before returning it
  int get priority => _priority;

  String get date => _date;

  String get description => _description;

  String get title => _title;

  int get id => _id;


  //SETTERS - We perform some checks before setting to ensure the value input is valid.
  set priority(int value) {
    if(value > 0 && value <= 3){
      _priority = value;
    }
  }

  set date(String value) {
    _date = value;
  }

  set description(String value) {
    if(value.length <= 255){
      _description = value;
    }

  }

  set title(String value) {
    if(value.length <= 255) {
      _title = value;
    }
  }


  /*A MAP METHOD: this method returns a Map with a String as its key and dynamic type its as value.
                  Here we map the values of the class properties to the string keys. We also check if the id
                  is null before mapping*/
  Map<String, dynamic> toMap(){
    var map = Map<String, dynamic>();
    map['title'] = _title;
    map['description'] = _description;
    map['priority'] = _priority;
    map['date'] = _date;
    if(_id != null) {
      map['id'] = _id;
    }
    return map;
  }


  //NAMED CONSTRUCTOR: does the opposite of the map method, taking in any object and transforming it to an TODO object
  Todo.fromObject(dynamic o) {
    this._id = o['id'];
    this._title = o['title'];
    this._description = o['description'];
    this._priority = o['priority'];
    this._date = o['date'];
  }


}